with
ko as
(
select 1 id, 'Север' name  union all
select 2 id, 'Запад' name union all
select 3 id, 'Восток' name union all
select 4 id, 'Юг' name
),
ac as
(
select 1 ko, convert(datetime, '01.01.2023', 104) oper_date, 100 amount, 1 direction union all
select 1 ko, convert(datetime, '11.01.2023', 104) oper_date, 30 amount, 0 direction union all
select 1 ko, convert(datetime, '01.02.2023', 104) oper_date, 230 amount, 1 direction union all
select 1 ko, convert(datetime, '11.02.2023', 104) oper_date, 40 amount, 0 direction union all
select 1 ko, convert(datetime, '01.03.2023', 104) oper_date, 90 amount, 1 direction union all
select 1 ko, convert(datetime, '11.03.2023', 104) oper_date, 180 amount, 0 direction union all
select 1 ko, convert(datetime, '01.04.2023', 104) oper_date, 400 amount, 1 direction union all
select 1 ko, convert(datetime, '11.04.2023', 104) oper_date, 100 amount, 0 direction union all
select 1 ko, convert(datetime, '01.05.2023', 104) oper_date, 120 amount, 1 direction union all
select 1 ko, convert(datetime, '11.05.2023', 104) oper_date, 310 amount, 0 direction union all
select 1 ko, convert(datetime, '01.06.2023', 104) oper_date, 100 amount, 1 direction union all
select 1 ko, convert(datetime, '11.06.2023', 104) oper_date, 40 amount, 0 direction union all
select 1 ko, convert(datetime, '01.07.2023', 104) oper_date, 90 amount, 1 direction union all
select 1 ko, convert(datetime, '11.07.2023', 104) oper_date, 180 amount, 0 direction union all
select 1 ko, convert(datetime, '01.08.2023', 104) oper_date, 400 amount, 1 direction union all
select 1 ko, convert(datetime, '11.08.2023', 104) oper_date, 100 amount, 0 direction union all
select 1 ko, convert(datetime, '01.09.2023', 104) oper_date, 120 amount, 1 direction union all
select 2 ko, convert(datetime, '01.02.2022', 104) oper_date, 725 amount, 1 direction union all
select 2 ko, convert(datetime, '11.02.2023', 104) oper_date, 40 amount, 0 direction union all
select 2 ko, convert(datetime, '01.03.2023', 104) oper_date, 90 amount, 1 direction union all
select 2 ko, convert(datetime, '11.03.2023', 104) oper_date, 180 amount, 0 direction union all
select 2 ko, convert(datetime, '01.04.2023', 104) oper_date, 100 amount, 1 direction union all
select 2 ko, convert(datetime, '11.04.2022', 104) oper_date, 380 amount, 0 direction union all
select 2 ko, convert(datetime, '01.12.2022', 104) oper_date, 120 amount, 1 direction union all
select 2 ko, convert(datetime, '11.12.2022', 104) oper_date, 480 amount, 0 direction union all
select 2 ko, convert(datetime, '01.12.2022', 104) oper_date, 80 amount, 1 direction union all
select 3 ko, convert(datetime, '01.01.2023', 104) oper_date, 125 amount, 1 direction union all
select 3 ko, convert(datetime, '11.02.2023', 104) oper_date, 40 amount, 0 direction union all
select 3 ko, convert(datetime, '01.03.2023', 104) oper_date, 90 amount, 1 direction union all
select 3 ko, convert(datetime, '11.03.2023', 104) oper_date, 180 amount, 0 direction union all
select 3 ko, convert(datetime, '01.11.2022', 104) oper_date, 100 amount, 1 direction union all
select 3 ko, convert(datetime, '11.10.2022', 104) oper_date, 80 amount, 0 direction union all
select 3 ko, convert(datetime, '01.08.2023', 104) oper_date, 120 amount, 1 direction union all
select 3 ko, convert(datetime, '11.08.2023', 104) oper_date, 10 amount, 0 direction union all
select 3 ko, convert(datetime, '01.09.2023', 104) oper_date, 80 amount, 1 direction union all
select 4 ko, convert(datetime, '01.02.2023', 104) oper_date, 90 amount, 0 direction union all
select 4 ko, convert(datetime, '11.03.2023', 104) oper_date, 50 amount, 1 direction union all
select 4 ko, convert(datetime, '01.12.2022', 104) oper_date, 100 amount, 0 direction union all
select 4 ko, convert(datetime, '01.12.2022', 104) oper_date, 120 amount, 1 direction union all
select 4 ko, convert(datetime, '11.10.2022', 104) oper_date, 480 amount, 1 direction union all
select 4 ko, convert(datetime, '01.11.2022', 104) oper_date, 80 amount, 1 direction
),
c5 as
(
SELECT DISTINCT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(DATEADD(month, -5, GETDATE())) = MONTH(oper_date)
GROUP BY ko
),
c4 as
(
SELECT DISTINCT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(DATEADD(month, -4, GETDATE())) = MONTH(oper_date)
GROUP BY ko
),
c3 as
(
SELECT DISTINCT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(DATEADD(month, -3, GETDATE())) = MONTH(oper_date)
GROUP BY ko
),
c2 as
(
SELECT DISTINCT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(DATEADD(month, -2, GETDATE())) = MONTH(oper_date)
GROUP BY ko
),
c1 as
(
SELECT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(DATEADD(month, -1, GETDATE())) = MONTH(oper_date)
GROUP BY ko
),
c0 as
(SELECT DISTINCT ko,
	SUM(CASE WHEN direction = 1 THEN amount ELSE -amount END) summa
FROM ac
WHERE MONTH(GETDATE()) = MONTH(oper_date)
GROUP BY ko
),
s5 as
(SELECT DISTINCT ac.ko, c5.summa
FROM ac
JOIN c5 ON ac.ko=c5.ko
),
s4 as
(SELECT DISTINCT ac.ko, c4.summa
FROM ac
JOIN c4 ON ac.ko=c4.ko
),
s3 as
(SELECT DISTINCT ac.ko, c3.summa
FROM ac
JOIN c3 ON ac.ko=c3.ko
),
s2 as
(SELECT DISTINCT ac.ko, c2.summa
FROM ac
JOIN c2 ON ac.ko=c2.ko
),
s1 as
(SELECT DISTINCT ac.ko, c1.summa
FROM ac
JOIN c1 ON ac.ko=c1.ko
),
s0 as
(SELECT DISTINCT ac.ko, c0.summa
FROM ac
JOIN c0 ON ac.ko=c0.ko
)
SELECT 'Контрагент' 'name',
		DATENAME(month, DATEADD(month, -5, GETDATE())) M5,
		DATENAME(month, DATEADD(month, -4, GETDATE())) M4,
		DATENAME(month, DATEADD(month, -3, GETDATE())) M3,
		DATENAME(month, DATEADD(month, -2, GETDATE())) M2,
		DATENAME(month, DATEADD(month, -1, GETDATE())) M1,
		DATENAME(month, GETDATE()) M0
UNION
SELECT
	ko.name as 'name',
	CONVERT(nvarchar, CASE WHEN c5.summa IS NULL THEN 0 ELSE c5.summa END) as M5,
	CONVERT(nvarchar, CASE WHEN c4.summa IS NULL THEN 0 ELSE c4.summa END) as M4,
	CONVERT(nvarchar, CASE WHEN c3.summa IS NULL THEN 0 ELSE c3.summa END) as M3,
	CONVERT(nvarchar, CASE WHEN c2.summa IS NULL THEN 0 ELSE c2.summa END) as M2,
	CONVERT(nvarchar, CASE WHEN c1.summa IS NULL THEN 0 ELSE c1.summa END) as M1,
	CONVERT(nvarchar, CASE WHEN c0.summa IS NULL THEN 0 ELSE c0.summa END) as M0
FROM ac
LEFT JOIN ko ON ac.ko = ko.id
LEFT JOIN c5 ON ac.ko = c5.ko
LEFT JOIN c4 ON ac.ko = c4.ko
LEFT JOIN c3 ON ac.ko = c3.ko
LEFT JOIN c2 ON ac.ko = c2.ko
LEFT JOIN c1 ON ac.ko = c1.ko
LEFT JOIN c0 ON ac.ko = c0.ko
UNION
SELECT
	'Итого:' as 'name',
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s5)) as M5,
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s4)) as M4,
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s3)) as M3,
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s2)) as M2,
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s1)) as M1,
	CONVERT(nvarchar,
			(SELECT SUM(summa) FROM s0)) as M0
FROM ac
LEFT JOIN s5 ON ac.ko = s5.ko
LEFT JOIN s4 ON ac.ko = s4.ko
LEFT JOIN s3 ON ac.ko = s3.ko
LEFT JOIN s2 ON ac.ko = s2.ko
LEFT JOIN s1 ON ac.ko = s1.ko
LEFT JOIN s0 ON ac.ko = s0.ko
ORDER BY M0 DESC
